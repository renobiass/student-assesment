import { studentAssesmentData } from "@/data";

export const downloadJsonFile = (jsonData) => {
  const fileName = "student-assesment";
  const json = JSON.stringify(jsonData, null, 2);
  const blob = new Blob([json], { type: "application/json" });
  const href = URL.createObjectURL(blob);

  // create "a" HTLM element with href to file
  const link = document.createElement("a");
  link.href = href;
  link.download = fileName + ".json";
  document.body.appendChild(link);
  link.click();

  // clean up "a" element & remove ObjectURL
  document.body.removeChild(link);
  URL.revokeObjectURL(href);
}

export const convertAssesmentDataToCorrespondingJson = (studentAssesmentData = studentAssesmentData) => {
  const aspects = {};

  studentAssesmentData[0].assesmentByAspect.forEach((aspect, indexx) => {
    const studentsScoreByAspect = {};
    studentAssesmentData.forEach((data, index) => {
      studentsScoreByAspect[studentAssesmentData[index].student.name.replace(" ", "_")] = studentAssesmentData[index].assesmentByAspect[indexx].score
    })
    aspects[`aspek_penilaian_${aspect.aspectId}`] = studentsScoreByAspect;
  });

  return aspects;
}

export const assesmentGrades = [1,2,3,4,5,6,7,8,9,10]