import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.scss'
import BootstrapTable from 'react-bootstrap-table-next'
import { useMemo, useState } from 'react'
import { studentAssesmentData } from '@/data'
import { assesmentGrades, convertAssesmentDataToCorrespondingJson, downloadJsonFile } from '@/utility'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  const [data, setData] = useState(studentAssesmentData);
  const COLUMNS = useMemo(() => {
    return [
      {
        dataField: 'id',
        text: 'No.',
        align: 'center',
        headerAlign: 'center',
        headerClasses: 'col',
        formatter: (cell, row, index) => {
          return (
            <div style={{ minWidth: 50 }}>
              <text>
                {cell}
              </text>
            </div>
          );
        },
      },
      {
        dataField: 'student.name',
        text: 'Nama Mahasiswa',
        headerClasses: 'col',
        align: 'center',
        headerAlign: 'center',
        formatter: (cell, row, index) => {
          return (
            <div style={{ minWidth: '150px' }}>
              <text>{cell}</text>
            </div>
          );
        },
      },
      {
        dataField: 'assesmentByAspect',
        text: 'Aspek Penilaian 1',
        headerClasses: 'col',
        align: 'center',
        headerAlign: 'center',
        formatExtraData: data,
        formatter: (cell, row, rowIndex, formatExtraData) => {
          return (
            <div style={{ minWidth: '130px' }}>
              <select class="form-select" aria-label="Default select example" onChange={(e) => {
                // const newData = [...formatExtraData];
                // newData[rowIndex].assesmentByAspect[0].score = e.target.value
                // setData(newData)
                formatExtraData[rowIndex].assesmentByAspect[0].score = e.target.value
              }}>
                <option selected>-- Input nilai ---</option>
                {
                  assesmentGrades.map((item, index) => {
                    return (<option key={index} value={item}>{item}</option>)
                  })
                }
              </select>
            </div>
          );
        },
      },
      {
        dataField: 'assesmentByAspect',
        text: 'Aspek Penilaian 2',
        headerClasses: 'col',
        align: 'center',
        headerAlign: 'center',
        formatExtraData: data,
        formatter: (cell, row, rowIndex, formatExtraData) => {
          const assesmentScore = cell[1].score;
          return (
            <div style={{ minWidth: '130px' }}>
              <select class="form-select" aria-label="Default select example" onChange={(e) => {
                formatExtraData[rowIndex].assesmentByAspect[1].score = e.target.value
              }}>
                <option selected>-- Input nilai ---</option>
                {
                  assesmentGrades.map((item, index) => {
                    return (<option key={index} value={item}>{item}</option>)
                  })
                }
              </select>
            </div>
          );
        },
      },
      {
        dataField: 'assesmentByAspect',
        text: 'Aspek Penilaian 3',
        headerClasses: 'col',
        align: 'center',
        headerAlign: 'center',
        formatExtraData: data,
        formatter: (cell, row, rowIndex, formatExtraData) => {
          const assesmentScore = cell[2].score;
          return (
            <div style={{ minWidth: '130px' }}>
              <select class="form-select" aria-label="Default select example" onChange={(e) => {
                formatExtraData[rowIndex].assesmentByAspect[2].score = e.target.value
              }}>
                <option selected>-- Input nilai ---</option>
                {
                  assesmentGrades.map((item, index) => {
                    return (<option key={index} value={item}>{item}</option>)
                  })
                }
              </select>
            </div>
          );
        },
      },
      {
        dataField: 'assesmentByAspect',
        text: 'Aspek Penilaian 4',
        headerClasses: 'col',
        align: 'center',
        headerAlign: 'center',
        formatExtraData: data,
        formatter: (cell, row, rowIndex, formatExtraData) => {
          const assesmentScore = cell[3].score;
          return (
            <div style={{ minWidth: '130px' }}>
              <select class="form-select" aria-label="Default select example" onChange={(e) => {
                formatExtraData[rowIndex].assesmentByAspect[3].score = e.target.value
              }}>
                <option selected>-- Input nilai ---</option>
                {
                  assesmentGrades.map((item, index) => {
                    return (<option key={index} value={item}>{item}</option>)
                  })
                }
              </select>
            </div>
          );
        },
      },
      {
        dataField: 'assesmentByAspect',
        text: 'Aspek Penilaian 5',
        headerClasses: 'col',
        align: 'center',
        headerAlign: 'center',
        formatExtraData: data,
        formatter: (cell, row, rowIndex, formatExtraData) => {
          const assesmentScore = cell[4].score;
          return (
            <div style={{ minWidth: '130px' }}>
              <select class="form-select" aria-label="Default select example" onChange={(e) => {
                formatExtraData[rowIndex].assesmentByAspect[4].score = e.target.value
              }}>
                <option selected>-- Input nilai ---</option>
                {
                  assesmentGrades.map((item, index) => {
                    return (<option key={index} value={item}>{item}</option>)
                  })
                }
              </select>
            </div>
          );
        },
      },
      {
        dataField: 'assesmentByAspect',
        text: 'Aspek Penilaian 6',
        headerClasses: 'col',
        align: 'center',
        headerAlign: 'center',
        formatExtraData: data,
        formatter: (cell, row, rowIndex, formatExtraData) => {
          const assesmentScore = cell[5].score;
          return (
            <div style={{ minWidth: '130px' }}>
              <select class="form-select" aria-label="Default select example" onChange={(e) => {
                formatExtraData[rowIndex].assesmentByAspect[5].score = e.target.value
              }}>
                <option selected>-- Input nilai ---</option>
                {
                  assesmentGrades.map((item, index) => {
                    return (<option key={index} value={item}>{item}</option>)
                  })
                }
              </select>
            </div>
          );
        },
      },
      {
        dataField: 'assesmentByAspect',
        text: 'Aspek Penilaian 7',
        headerClasses: 'col',
        align: 'center',
        headerAlign: 'center',
        formatExtraData: data,
        formatter: (cell, row, rowIndex, formatExtraData) => {
          const assesmentScore = cell[6].score;
          return (
            <div style={{ minWidth: '130px' }}>
              <select class="form-select" aria-label="Default select example" onChange={(e) => {
                formatExtraData[rowIndex].assesmentByAspect[6].score = e.target.value
              }}>
                <option selected>-- Input nilai ---</option>
                {
                  assesmentGrades.map((item, index) => {
                    return (<option key={index} value={item}>{item}</option>)
                  })
                }
              </select>
            </div>
          );
        },
      },
      {
        dataField: 'assesmentByAspect',
        text: 'Aspek Penilaian 8',
        headerClasses: 'col',
        align: 'center',
        headerAlign: 'center',
        formatExtraData: data,
        formatter: (cell, row, rowIndex, formatExtraData) => {
          const assesmentScore = cell[7].score;
          return (
            <div style={{ minWidth: '130px' }}>
              <select class="form-select" aria-label="Default select example" onChange={(e) => {
                formatExtraData[rowIndex].assesmentByAspect[7].score = e.target.value
              }}>
                <option selected>-- Input nilai ---</option>
                {
                  assesmentGrades.map((item, index) => {
                    return (<option key={index} value={item}>{item}</option>)
                  })
                }
              </select>
            </div>
          );
        },
      },
      {
        dataField: 'assesmentByAspect',
        text: 'Aspek Penilaian 9',
        headerClasses: 'col',
        align: 'center',
        headerAlign: 'center',
        formatExtraData: data,
        formatter: (cell, row, rowIndex, formatExtraData) => {
          const assesmentScore = cell[8].score;
          return (
            <div style={{ minWidth: '130px' }}>
              <select class="form-select" aria-label="Default select example" onChange={(e) => {
                formatExtraData[rowIndex].assesmentByAspect[8].score = e.target.value
              }}>
                <option selected>-- Input nilai ---</option>
                {
                  assesmentGrades.map((item, index) => {
                    return (<option key={index} value={item}>{item}</option>)
                  })
                }
              </select>
            </div>
          );
        },
      },
      {
        dataField: 'assesmentByAspect',
        text: 'Aspek Penilaian 10',
        headerClasses: 'col',
        align: 'center',
        headerAlign: 'center',
        formatExtraData: data,
        formatter: (cell, row, rowIndex, formatExtraData) => {
          const assesmentScore = cell[9].score;
          return (
            <div style={{ minWidth: '130px' }}>
              <select class="form-select" aria-label="Default select example" onChange={(e) => {
                formatExtraData[rowIndex].assesmentByAspect[9].score = e.target.value
              }}>
                <option selected>-- Input nilai ---</option>
                {
                  assesmentGrades.map((item, index) => {
                    return (<option key={index} value={item}>{item}</option>)
                  })
                }
              </select>
            </div>
          );
        },
      },
    ]
  }, [])

  return (
    <>
      <Head>
        <title>Aplikasi Penilaian Mahasiswa</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={`${styles.main} ${inter.className}`}>
        <div className={styles.description}>
          <h2>
            Aplikasi Penilaian Mahasiswa&nbsp;
          </h2>
        </div>
        <div style={{ marginTop: 25 }}>
          <BootstrapTable
            data={data}
            columns={COLUMNS}
            keyField='id'
            wrapperClasses='my-table'
            rowClasses="table-row"
            headerClasses="table-heading"
            noDataIndication="No data"
          />
        </div>
        <div style={{ marginTop: 20 }}>
          <button
            style={{ 
              backgroundColor: '#20553e', 
              color: 'white', 
              padding: '10px 20px',}}
            className={styles.description}
            onClick={() => {
              const jsonData = convertAssesmentDataToCorrespondingJson(data)
              downloadJsonFile(jsonData)
            }}
          >Simpan</button>
        </div>
      </main>
    </>
  )
}