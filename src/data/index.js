export const studentAssesmentData = [
    {
        id: 1,
        student: {
            id: 1,
            name: "mahasiswa 1"
        },
        assesmentByAspect: [
            {
                aspectId: 1,
                score: '',
            },
            {
                aspectId: 2,
                score: '',
            },
            {
                aspectId: 3,
                score: '',
            },
            {
                aspectId: 4,
                score: '',
            },
            {
                aspectId: 5,
                score: '',
            },
            {
                aspectId: 6,
                score: '',
            },
            {
                aspectId: 7,
                score: '',
            },
            {
                aspectId: 8,
                score: '',
            },
            {
                aspectId: 9,
                score: '',
            },
            {
                aspectId: 10,
                score: '',
            },
        ]
    },
    {
        id: 2,
        student: {
            id: 2,
            name: "mahasiswa 2"
        },
        assesmentByAspect: [
            {
                aspectId: 1,
                score: '',
            },
            {
                aspectId: 2,
                score: '',
            },
            {
                aspectId: 3,
                score: '',
            },
            {
                aspectId: 4,
                score: '',
            },
            {
                aspectId: 5,
                score: '',
            },
            {
                aspectId: 6,
                score: '',
            },
            {
                aspectId: 7,
                score: '',
            },
            {
                aspectId: 8,
                score: '',
            },
            {
                aspectId: 9,
                score: '',
            },
            {
                aspectId: 10,
                score: '',
            },
        ]
    },
    {
        id: 3,
        student: {
            id: 3,
            name: "mahasiswa 3"
        },
        assesmentByAspect: [
            {
                aspectId: 1,
                score: '',
            },
            {
                aspectId: 2,
                score: '',
            },
            {
                aspectId: 3,
                score: '',
            },
            {
                aspectId: 4,
                score: '',
            },
            {
                aspectId: 5,
                score: '',
            },
            {
                aspectId: 6,
                score: '',
            },
            {
                aspectId: 7,
                score: '',
            },
            {
                aspectId: 8,
                score: '',
            },
            {
                aspectId: 9,
                score: '',
            },
            {
                aspectId: 10,
                score: '',
            },
        ]
    },
    {
        id: 4,
        student: {
            id: 4,
            name: "mahasiswa 4"
        },
        assesmentByAspect: [
            {
                aspectId: 1,
                score: '',
            },
            {
                aspectId: 2,
                score: '',
            },
            {
                aspectId: 3,
                score: '',
            },
            {
                aspectId: 4,
                score: '',
            },
            {
                aspectId: 5,
                score: '',
            },
            {
                aspectId: 6,
                score: '',
            },
            {
                aspectId: 7,
                score: '',
            },
            {
                aspectId: 8,
                score: '',
            },
            {
                aspectId: 9,
                score: '',
            },
            {
                aspectId: 10,
                score: '',
            },
        ]
    },
    {
        id: 5,
        student: {
            id: 5,
            name: "mahasiswa 5"
        },
        assesmentByAspect: [
            {
                aspectId: 1,
                score: '',
            },
            {
                aspectId: 2,
                score: '',
            },
            {
                aspectId: 3,
                score: '',
            },
            {
                aspectId: 4,
                score: '',
            },
            {
                aspectId: 5,
                score: '',
            },
            {
                aspectId: 6,
                score: '',
            },
            {
                aspectId: 7,
                score: '',
            },
            {
                aspectId: 8,
                score: '',
            },
            {
                aspectId: 9,
                score: '',
            },
            {
                aspectId: 10,
                score: '',
            },
        ]
    },
    {
        id: 6,
        student: {
            id: 6,
            name: "mahasiswa 6"
        },
        assesmentByAspect: [
            {
                aspectId: 1,
                score: '',
            },
            {
                aspectId: 2,
                score: '',
            },
            {
                aspectId: 3,
                score: '',
            },
            {
                aspectId: 4,
                score: '',
            },
            {
                aspectId: 5,
                score: '',
            },
            {
                aspectId: 6,
                score: '',
            },
            {
                aspectId: 7,
                score: '',
            },
            {
                aspectId: 8,
                score: '',
            },
            {
                aspectId: 9,
                score: '',
            },
            {
                aspectId: 10,
                score: '',
            },
        ]
    },
    {
        id: 7,
        student: {
            id: 7,
            name: "mahasiswa 7"
        },
        assesmentByAspect: [
            {
                aspectId: 1,
                score: '',
            },
            {
                aspectId: 2,
                score: '',
            },
            {
                aspectId: 3,
                score: '',
            },
            {
                aspectId: 4,
                score: '',
            },
            {
                aspectId: 5,
                score: '',
            },
            {
                aspectId: 6,
                score: '',
            },
            {
                aspectId: 7,
                score: '',
            },
            {
                aspectId: 8,
                score: '',
            },
            {
                aspectId: 9,
                score: '',
            },
            {
                aspectId: 10,
                score: '',
            },
        ]
    },
    {
        id: 8,
        student: {
            id: 8,
            name: "mahasiswa 8"
        },
        assesmentByAspect: [
            {
                aspectId: 1,
                score: '',
            },
            {
                aspectId: 2,
                score: '',
            },
            {
                aspectId: 3,
                score: '',
            },
            {
                aspectId: 4,
                score: '',
            },
            {
                aspectId: 5,
                score: '',
            },
            {
                aspectId: 6,
                score: '',
            },
            {
                aspectId: 7,
                score: '',
            },
            {
                aspectId: 8,
                score: '',
            },
            {
                aspectId: 9,
                score: '',
            },
            {
                aspectId: 10,
                score: '',
            },
        ]
    },
    {
        id: 9,
        student: {
            id: 9,
            name: "mahasiswa 9"
        },
        assesmentByAspect: [
            {
                aspectId: 1,
                score: '',
            },
            {
                aspectId: 2,
                score: '',
            },
            {
                aspectId: 3,
                score: '',
            },
            {
                aspectId: 4,
                score: '',
            },
            {
                aspectId: 5,
                score: '',
            },
            {
                aspectId: 6,
                score: '',
            },
            {
                aspectId: 7,
                score: '',
            },
            {
                aspectId: 8,
                score: '',
            },
            {
                aspectId: 9,
                score: '',
            },
            {
                aspectId: 10,
                score: '',
            },
        ]
    },
    {
        id: 10,
        student: {
            id: 10,
            name: "mahasiswa 10"
        },
        assesmentByAspect: [
            {
                aspectId: 1,
                score: '',
            },
            {
                aspectId: 2,
                score: '',
            },
            {
                aspectId: 3,
                score: '',
            },
            {
                aspectId: 4,
                score: '',
            },
            {
                aspectId: 5,
                score: '',
            },
            {
                aspectId: 6,
                score: '',
            },
            {
                aspectId: 7,
                score: '',
            },
            {
                aspectId: 8,
                score: '',
            },
            {
                aspectId: 9,
                score: '',
            },
            {
                aspectId: 10,
                score: '',
            },
        ]
    },
]